Test proyect made in order to test Vue js user, groups and permission handled in the state with Vuex and routing. Login system created with Node Sails JS and mongodb.

# Setup
Run npm install in each sub folder: vue-pr and api-rest.

# Proyect

- Install and set sails empty app. ```npm -g install sails```
- Configure API ```sails new app```
- Create API for each model ```sails generate api modelname```
- Connect Sails app with MongoDB ```npm install sails-mongo```
  * in config/connections.js uncomment section related to mongo and enter necessary info there
  * in config/models.js enter you connection adapter variable
  * develop mode? drop db ```sails.config.models.migrate = 'drop';```
- Install vue using vue-cli


# Run
- Run mongo: ```mongoDbFolder: ./bin/mongod```
- Sails Api REST ```api-rest: sails lift```
    * Postman export included
    * CORS allowed host localhost:8080
- Vue js ```vue-pr: npm run dev```


Sails, node, axios
Vuejs, Vuex, namespacing, pug, scss and their webpack loaders
Postman