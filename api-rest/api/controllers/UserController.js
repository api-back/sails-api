/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  login: async function (req, res) {
    const name = req.body['user'];
    const password = req.body['password'];
    
    if (name && password) {
      const userObject = await User.findOne({name,password});
      console.debug(name, password);
      if (userObject) res.send(userObject);
    }
    return  res.forbidden();
  }
}

// module.exports = {

//   friendlyName: 'login',

//   description: 'Look up the specified user and welcome them, or redirect to a signup page if no user was found.',

//   inputs: {
//      user: {
//        description: 'Username',
//        type: 'string',
//        required: true
//      },
//      password: {
//       description: 'Username',
//       type: 'string',
//       required: true
//      }
//   },

//   exits: {
//      success: {
//       //  responseType: 'view',
//       //  viewTemplatePath: 'pages/welcome'
//      },
//      notFound: {
//       //  description: 'No user with the specified ID was found in the database.',
//       //  responseType: 'notFound'
//      }
//   },

//   fn: async function (inputs, exits) {
//      var user = await User.findOne({ name: inputs.user, password: inputs.password });
//      if (!user) { return exits.notFound(); }
//      return exits.success({name: user.name});
//   }
// };