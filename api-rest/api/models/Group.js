/**
 * Group.js
 */

module.exports = {
  attributes: {

    name: {
      type: 'string',
      unique: true,
      required: true
    },
    permissions: {
      collection: 'permission'
    }

  },

};

