/**
 * User.js
 */

module.exports = {
  attributes: {

    // primitives
    name: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true,
    },
    groups: {
      // kinda of relation between models
      collection: 'group',
    },
    permissions: {
      collection: 'permission'
    },

  },
};
// develop mode? drop db
// sails.config.models.migrate = 'drop';
sails.config.models.migrate = 'alter';

