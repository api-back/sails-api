/**
 * Permission.js
 */

module.exports = {
  attributes: {

    code: {
      type: 'String',
      required: true,
      unique: true
    }

  },

};

